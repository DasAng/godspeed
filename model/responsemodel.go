package model

type ResponseModel struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	ErrorCode int `json:"errorCode"`
}
