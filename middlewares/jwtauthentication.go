package middlewares

import (
	"net/http"
	"gitlab.com/DasAng/godspeed/logger"
	jwt "github.com/dgrijalva/jwt-go"
	"fmt"
	"gitlab.com/DasAng/godspeed/model"
	"encoding/json"
	"gitlab.com/DasAng/godspeed/commons"
)

func JwtAuthentication(jwtSecret string,next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter,r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		clientIp := struct {
			ClientIp string
		}{
			r.RemoteAddr,
		}
		token, err := jwt.ParseFromRequest(r,func(token *jwt.Token) (interface{}, error) {
			return []byte(jwtSecret),nil
		})
		if err == nil && token.Valid {
			next.ServeHTTP(w,r)
		} else {
			if err != nil {
				logger.Log.Errorf(fmt.Sprintf("Unauthorized access. %s",err.Error()),clientIp)
			} else {
				logger.Log.Errorf(fmt.Sprintf("Unauthorized access. Jwt token is not valid"),clientIp)
			}

			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(&model.ResponseModel{Message: "Unauthorized access",
				Code: int(http.StatusUnauthorized),
				ErrorCode:commons.ERROR_UNAUTHORIZED_ACCESS})
		}

	})
}
